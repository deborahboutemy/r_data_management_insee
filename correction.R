#########################################
#
# Exercice : Manipulation des fichiers INSEE
# Objetifs :
# - data management
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des packages
library(dplyr)
library(lubridate)
library(readxl)


## Chemins
source("path.R")

## Fonctions
taux = function(numerateur, denominateur, precision = 2){
return(round(numerateur / denominateur * 100, precision))
}

## Variables globales
# Variables
codes_lyon = c("69381", "69382", "69383", "69384", "69385", "69386", "69387", "69388", "69389")
codes_marseille = c("13201", "13202", "13203", "13204", "13205", "13206", "13207", "13208", "132019",
                    "13210", "13211", "13212", "13213", "13214", "13215", "13216")
codes_paris = c("75101", "75102", "75103", "75104", "75105", "75106", "75107", "75108", "75109", "75110",
                "75111", "75112", "75113", "75114", "75115", "75116", "75117", "75118", "75119", "75120")
codes_postaux_arrondissements = c(codes_lyon, codes_marseille, codes_paris)

# ------------------------------------------------------------------------------------------

list.files(path_import)


############################################################################################
# 1 Chargement des données
############################################################################################

## Fichier Population
age_pop_raw = read.csv2(file.path(path_import, 'BTT_TD_POP1B_2017.csv'), dec = '.')

## Fichier décès
deces_raw = read.csv2(file.path(path_import, 'deces-2017.csv'))

## Immigration
pop_img_raw = readxl::read_xlsx(file.path(path_import,"BTX_TD_IMG1A_2017.xlsx")) # Immigration INSEE

## Statuts professionnels
csp_raw = readxl::read_xlsx(file.path(path_import, "TCRD_005.xlsx"), sheet = 'DEP')


############################################################################################
# 2 Exploration des fichiers
############################################################################################

str(age_pop_raw)
head(age_pop_raw, 10)

str(deces_raw)

str(pop_img_raw)

str(csp_raw)

############################################################################################
# 2 Nettoyage données
############################################################################################

# Population insee
# -----------------------------------------------------------------------

# Passer les noms de colonnes en minuscules
colnames(age_pop_raw) = tolower(colnames(age_pop_raw))

# Ajout colonne département
age_pop_departement = age_pop_raw %>%
  filter(!codgeo %in% codes_postaux_arrondissements) %>% # kdkdkdkkdkdkdkd
  dplyr::mutate(departement = substr(codgeo, 1, 2),
                 somme_age = aged100 * nb) %>%
  dplyr::filter(departement != "") %>%
  dplyr::group_by(departement) %>%
  dplyr::summarise(
    nb_habitants = round(sum(nb,na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
    ) %>%
  mutate(taux_hommes_vivants = round(nb_hommes / (nb_hommes + nb_femmes), 2),
         age_moyen_vivant = round((somme_age / nb_habitants), 2))

# Décès INSEE
# -----------------------------------------------------------------------

deces_departement = deces_raw %>%
  mutate(departement = substr(lieudeces, 1, 2),
         datenaiss_hhhh = ymd(datenaiss),
         datedeces = ymd(datedeces),
         age_deces = time_length(difftime(datedeces, datenaiss), "years")) %>%
  dplyr::filter(departement != "") %>%
  group_by(departement) %>%
  summarize(nb_deces = n(),
            age_moyen_deces = mean(age_deces, na.rm = TRUE))

deces_departement %>% arrange(age_moyen_deces)

# Img
# -----------------------------------------------------------------------
colnames(pop_img_raw) = tolower(colnames(pop_img_raw))

img_departement = pop_img_raw %>%
  mutate(departement = substr(codgeo, 1, 2)) %>%
  group_by(departement) %>%
  summarize(
    nb_non_img_hom = round(sum(age400_immi2_sexe1 + age415_immi2_sexe1 + age425_immi2_sexe1 + age455_immi2_sexe1, na.rm = TRUE)),
    nb_non_img_fem = round(sum(age400_immi2_sexe2 + age415_immi2_sexe2 + age425_immi2_sexe2 + age455_immi2_sexe2, na.rm = TRUE)),
    nb_img_hom = round(sum(age400_immi1_sexe1 + age415_immi1_sexe1 + age425_immi1_sexe1 + age455_immi1_sexe1, na.rm = TRUE)),
    nb_img_fem = round(sum(age400_immi1_sexe2 + age415_immi1_sexe2 + age425_immi1_sexe2 + age455_immi1_sexe2, na.rm = TRUE)),
    nb_non_img = round(sum(nb_non_img_hom + nb_non_img_fem, na.rm = TRUE)),
    nb_img = round(sum(nb_img_hom + nb_img_fem, na.rm = TRUE))
  ) %>%
  mutate(
    taux_img = round(nb_img / (nb_img + nb_non_img), 2)
  )  %>%
  select(departement, nb_non_img_hom, nb_non_img_fem, nb_img_hom, nb_img_fem, nb_non_img, nb_img, taux_img)

head(img_departement)


# Status pro
# -----------------------------------------------------------------------

colnames(csp_raw) = c('departement', 'lib_departement', 'taux_agriculteurs_exploitants', 'taux_artisans_commerçants_chef_entreprises', 'taux_cadres', 'taux_profs_intermediaires', 'taux_employés', 'taux_ouvriers', 'taux_retraités', 'taux_sans_act_pro')
head(csp_raw)
csp_departement = csp_raw[4:nrow(csp_raw),]
cols =  colnames(csp_departement)[!colnames(csp_departement) %in% c('departement', 'lib_departement')]
csp_departement[cols] = lapply(csp_departement[cols], as.numeric)

csp_departement = csp_departement %>%
  select(-lib_departement)

colnames(csp_departement)

# Merge
# -----------------------------------------------------------------------

pop = age_pop_departement %>%
  # Décès
  merge(deces_departement, by = 'departement') %>%
  #
  merge(img_departement, by = 'departement') %>%
  #
  merge(csp_departement, by = 'departement') %>%
  #
  merge(csp_departement, by = 'departement')

# Save the final dataset
# -----------------------------------------------------------------------

pop %>%
  write.csv2(file.path(path_export, 'pop.csv'), row.names = FALSE)
